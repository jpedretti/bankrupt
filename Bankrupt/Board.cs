﻿using Bankrupt.Players;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bankrupt
{
    class Board
    {
        const int MAX_TURNS = 1000;
        const int COINS_FOR_BOARD_COMPLETION = 100;
        readonly SixSidedDice _dice = new SixSidedDice();
        List<Property> _properties;
        List<Player> _players;

        public Board(IEnumerable<Property> properties, IEnumerable<Player> players)
        {
            Console.WriteLine("Configurando Tabuleiro\n");
            _properties = properties.ToList();
            var rand = new Random();
            _players = players.OrderBy(player => rand.Next()).ToList();
        }

        public PlayResult PlayGame()
        {
            PlayResult result = null;
            for (var turn = 1; turn <= MAX_TURNS; turn++)
            {
                Console.WriteLine($"\nTurno nº {turn}");
                foreach (var player in _players)
                {
                    Console.WriteLine($"Vez do jogador {player}");

                    var diceResult = player.ThrowDice(_dice);
                    Console.WriteLine($"O jogador {player} tirou o número {diceResult} no dado");
                    player.Position = CalculateNewPosition(player, diceResult, turn);
                    Console.WriteLine($"Nova posição do jogador {player}: {player.Position + 1}");

                    var property = _properties[player.Position];
                    if (property.HasOwner && !property.IsPlayerOwner(player))
                    {
                        PayRent(player, property);
                    }
                    else if (!property.HasOwner)
                    {
                        TryToBuyTheProperty(player, property);
                    }
                    else
                    {
                        Console.WriteLine($"A propriedade já é do jogador {player}");
                    }
                }

                Console.WriteLine("Fim de turno\n");
                RemovePlayersThatLost();

                if (_players.Count == 1)
                {
                    result = new PlayResult(_players.First(), turn);
                    break;
                }
            }

            return result ?? new PlayResult(_players.First(), MAX_TURNS, true);
        }

        private void RemovePlayersThatLost()
        {
            for (var index = 0; index < _players.Count; index++)
            {
                var player = _players[index];
                if (player.Coins < 0)
                {
                    Console.WriteLine($"{player} perdeu com {player.Coins} moedas\n");
                    var looserProperties = _properties.Where(property => property.Owner == player);
                    foreach (var property in looserProperties)
                    {
                        property.ClearOwner();
                    }
                    _players.RemoveAt(index);
                }
            }
        }

        private static void PayRent(Player player, Property property)
        {
            player.PayRent(property);
            property.Owner.ReceiveRent(property);
            Console.WriteLine($"{player} pagou {property.RentValue} para {property.Owner}");
        }

        private static void TryToBuyTheProperty(Player player, Property property)
        {
            var willPurchase = player.WillPurchaseProperty(property);
            if (willPurchase)
            {
                property.Purchase(player);
                Console.WriteLine($"{player} comprou a propriedade da posição {player.Position + 1}");
            }
            else
            {
                Console.WriteLine($"o jogador {player} não comprou a propriedade da posição {player.Position + 1}");
            }
        }

        private int CalculateNewPosition(Player player, int diceResult, int turn)
        {
            var newPosition = player.Position;
            if (player.Position + diceResult >= _properties.Count)
            {
                var diffToEnd = _properties.Count - player.Position;
                newPosition = diceResult - diffToEnd;
                if (turn > 0)
                {
                    Console.WriteLine($"O jogador {player} completou o tabuleiro e ganhou {COINS_FOR_BOARD_COMPLETION} coins");
                    player.ReceivCoins(COINS_FOR_BOARD_COMPLETION);
                }
            }
            else
            {
                newPosition += diceResult;
            }

            return newPosition;
        }
    }
}
