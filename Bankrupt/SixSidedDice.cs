﻿using System;

namespace Bankrupt
{
    public class SixSidedDice
    {
        Random _rand = new Random();
        public int Throw() => _rand.Next(1, 7); 
    }
}
