﻿using Bankrupt.Players;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bankrupt
{
    class Program
    {
        private const int MAX_PLAYS = 300;

        static void Main(string[] _)
        {
            Console.WriteLine("Iniciando Bankrupt\n");

            var properties = new PropertiesLoader().LoadProperties();

            var results = new List<PlayResult>();
            for (var plays = 1; plays <= MAX_PLAYS; plays++)
            {
                Console.WriteLine($"Iniciando Simulação nº {plays}\n");
                var players = new Player[] {
                    new CautiousPlayer(),
                    new ImpulsivePlayer(),
                    new PickyPlayer(),
                    new RandomPlayer(new Random())
                };
                var board = new Board(properties, players);
                results.Add(board.PlayGame());
                Console.WriteLine($"Simulação nº {plays} Termianda\n");
            }

            Console.WriteLine("Simulações terminadas. Calculando Resultados.\n");
            CalculateResults(results);

        }

        private static void CalculateResults(List<PlayResult> results)
        {
            Console.WriteLine($"Quantidade de partidas com Time Out: {results.Where(result => result.WasTimeOut).Count()}\n");
            Console.WriteLine($"Media de turnos da partida: {results.Average(result => result.Turns)}\n");

            var groupedWinners = results.GroupBy(result => result.Winner);
            foreach (var winnerGroup in groupedWinners)
            {
                PrintWinPercentage(winnerGroup.First().Winner, winnerGroup.Count());
            }

            var greaterGroup = groupedWinners.Aggregate((g1, g2) => g1.Count() > g2.Count() ? g1 : g2);
            var player = greaterGroup.First().Winner;
            Console.WriteLine($"Comportamento que mais venceu: {player}\n");
        }

        static void PrintWinPercentage(Player player, int wins) =>
            Console.WriteLine($"o jogador {player} venceu {wins * 100 / MAX_PLAYS}% das vezes.\n");
    }
}