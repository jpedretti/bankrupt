﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Bankrupt
{
    class PropertiesLoader
    {
        public IEnumerable<Property> LoadProperties()
        {
            Console.WriteLine("Lendo arquivo gameConfig.txt");
            var lines = File.ReadLines("gameConfig.txt");

            return lines.Select(line =>
            {
                var values = line.Split(' ');
                return new Property(int.Parse(values.First()), int.Parse(values.Last()));
            });
        }
    }
}
