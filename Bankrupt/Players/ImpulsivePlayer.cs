﻿using System;

namespace Bankrupt.Players
{
    class ImpulsivePlayer : Player
    {
        public override string Name => nameof(ImpulsivePlayer);

        public override bool WillPurchaseProperty(Property property) => PurchaseProperty(property, true);
    }
}
