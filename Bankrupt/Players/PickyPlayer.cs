﻿namespace Bankrupt.Players
{
    public class PickyPlayer : Player
    {
        public override string Name => nameof(PickyPlayer);

        public override bool WillPurchaseProperty(Property property) => 
            PurchaseProperty(property, property.RentValue > 50);
    }
}
