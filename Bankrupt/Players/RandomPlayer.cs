﻿using System;

namespace Bankrupt.Players
{
    public class RandomPlayer : Player
    {
        readonly Random _random;

        public RandomPlayer(Random random)
        {
            _random = random;
        }

        public override string Name => nameof(RandomPlayer);

        public override bool WillPurchaseProperty(Property property)
        {
            var shouldBuy = _random.Next(0, 2) == 1;
            return PurchaseProperty(property, shouldBuy);
        }
    }
}
