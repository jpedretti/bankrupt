﻿namespace Bankrupt.Players
{
    public abstract class Player
    {
        public int Coins { get; protected set; } = 300;

        public int Position { get; set; } = -1;

        public abstract string Name { get; }

        public int ThrowDice(SixSidedDice dice) => dice.Throw();

        public abstract bool WillPurchaseProperty(Property property);

        public void PayRent(Property property) => Coins -= property.RentValue;

        public void ReceiveRent(Property property) => Coins += property.RentValue;

        public void ReceivCoins(int coins) => Coins += coins;

        protected bool PurchaseProperty(Property property, bool shouldBuy)
        {
            var willBuy = shouldBuy && Coins >= property.PurchaseValue;
            if (willBuy)
            {
                Coins -= property.PurchaseValue;
            }

            return willBuy;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Player))
            {
                return false;
            }

            return Name == (obj as Player).Name;
        }

        public override int GetHashCode() => Name.GetHashCode();

        public override string ToString() => Name;
    }
}
