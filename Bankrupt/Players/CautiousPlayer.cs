﻿namespace Bankrupt.Players
{
    public class CautiousPlayer : Player
    {
        public override string Name => nameof(CautiousPlayer);

        public override bool WillPurchaseProperty(Property property) => 
            PurchaseProperty(property, Coins - property.PurchaseValue >= 80);
    }
}
