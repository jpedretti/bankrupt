﻿using Bankrupt.Players;
using System;

namespace Bankrupt
{
    class PlayResult
    {
        public Player Winner { get; set; }
        public int Turns { get; set; }
        public bool WasTimeOut { get; set; }

        public PlayResult(Player winner, int turns, bool wasTimeout = false)
        {
            Console.WriteLine($"{winner} é o vencedor");
            Winner = winner;
            Turns = turns;
            WasTimeOut = wasTimeout;
        }
    }
}
