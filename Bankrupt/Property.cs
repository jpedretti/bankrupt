﻿using Bankrupt.Players;

namespace Bankrupt
{
    public class Property
    {
        public Player Owner { get; private set; } = null;

        public bool HasOwner => Owner != null;

        public Property(int purchaseValue, int rentValue)
        {
            PurchaseValue = purchaseValue;
            RentValue = rentValue;
        }

        public int PurchaseValue { get; }

        public int RentValue { get; }

        public void Purchase(Player player) => Owner = player;

        public void ClearOwner() => Owner = null;

        public bool IsPlayerOwner(Player player) => player == Owner;
    }
}