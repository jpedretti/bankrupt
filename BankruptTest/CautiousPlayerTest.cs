using Bankrupt;
using Bankrupt.Players;
using Moq;
using NUnit.Framework;

namespace Tests
{
    public class CautiousPlayerTest
    {
        [Test]
        public void WillPurchaseProperty_CoinsMinusPurchaseValueLessThan80_ShouldReturnFalse()
        {
            var player = new CautiousPlayer();
            var property = new Property(250, 100);

            var willBuy = player.WillPurchaseProperty(property);

            Assert.False(willBuy);
            Assert.AreEqual(300, player.Coins);
        }

        [Test]
        public void WillPurchaseProperty_CoinsMinusPurchaseValueMoreThan80_ShouldReturnTrue()
        {
            var player = new CautiousPlayer();
            var property = new Property(220, 100);

            var willBuy = player.WillPurchaseProperty(property);

            Assert.True(willBuy);
            Assert.AreEqual(80, player.Coins);
        }
    }
}