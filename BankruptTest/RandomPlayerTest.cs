using Bankrupt;
using Bankrupt.Players;
using Moq;
using NUnit.Framework;
using System;

namespace Tests
{
    public class RandomPlayerTest
    {

        Mock<Random> _random = new Mock<Random>();

        [Test]
        public void WillPurchaseProperty_RandomReturnZeroAndMoney_ShouldReturnFalse()
        {
            _random.Setup(rand => rand.Next(It.IsAny<int>(), It.IsAny<int>())).Returns(0);
            var player = new RandomPlayer(_random.Object);
            var property = new Property(250, 100);

            var willBuy = player.WillPurchaseProperty(property);

            Assert.False(willBuy);
            Assert.AreEqual(300, player.Coins);
            _random.Verify(r => r.Next(0, 2));
        }

        [Test]
        public void WillPurchaseProperty_RandomReturnOneAndMoney_ShouldReturnTrue()
        {
            _random.Setup(rand => rand.Next(It.IsAny<int>(), It.IsAny<int>())).Returns(1);
            var player = new RandomPlayer(_random.Object);
            var property = new Property(220, 100);

            var willBuy = player.WillPurchaseProperty(property);

            Assert.True(willBuy);
            Assert.AreEqual(80, player.Coins);
            _random.Verify(r => r.Next(0, 2));
        }

        [Test]
        public void WillPurchaseProperty_RandomReturnZeroWithoutMoney_ShouldReturnFalse()
        {
            _random.Setup(rand => rand.Next(It.IsAny<int>(), It.IsAny<int>())).Returns(0);
            var player = new RandomPlayer(_random.Object);
            var property = new Property(400, 100);

            var willBuy = player.WillPurchaseProperty(property);

            Assert.False(willBuy);
            Assert.AreEqual(300, player.Coins);
            _random.Verify(r => r.Next(0, 2));
        }

        [Test]
        public void WillPurchaseProperty_RandomReturnOneWithoutMoney_ShouldReturnTrue()
        {
            _random.Setup(rand => rand.Next(It.IsAny<int>(), It.IsAny<int>())).Returns(1);
            var player = new RandomPlayer(_random.Object);
            var property = new Property(400, 100);

            var willBuy = player.WillPurchaseProperty(property);

            Assert.False(willBuy);
            Assert.AreEqual(300, player.Coins);
            _random.Verify(r => r.Next(0, 2));
        }
    }
}