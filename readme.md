# Desafio Tapps

## Necessário .Net Core 2.2

## Sem Visual Studio
### Restaurar Solution
> Na pasta da solution "Bankrupt"
>> Execute o comando "donet restore" 

### Execução do Bankrupt
> Entre na pasta do projeto "Bankrupt/Bankrupt"
>> Execute o comando "dotnet run" 

## Com Visual Studio
> Abrir a solution (Bankrupt.sln)
>> Pressionar F5